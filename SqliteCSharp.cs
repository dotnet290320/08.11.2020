using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0811sqlite
{
    class Program
    {
        // Nugget:
        // System.Data.SQLite
        // PM> Install-Package System.Data.SQLite -Version 1.0.113.6
        static void Main(string[] args)
        {
            // === Connection string 
            // MSSQL:
            // Data Source=.;Initial Catalog=Study;Integrated Security=True
            // Sqlite:
            // Data Source = D:\\SQLITE\\0411.db; Version = 3;
            // MySql:
            // Server=myServerAddress;Port=1234;Database=myDataBase;Uid=myUsername;Pwd=myPassword;
            using (SQLiteConnection conn = new SQLiteConnection("Data Source = D:\\SQLITE\\0411.db; Version = 3;"))
            {
                conn.Open();

                using (SQLiteCommand select_query = new SQLiteCommand("SELECT * from COMPANY", conn))
                {
                    using (SQLiteDataReader reader = select_query.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            // ID	NAME	AGE	ADDRESS	SALARY
                            Console.WriteLine($"{reader["ID"]} \t{reader["NAME"]} \t{reader["AGE"]} \t{reader["ADDRESS"]}     \t{reader["SALARY"]}");
                        }
                    }
                }
            }

        }
    }
}
