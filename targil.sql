/*
1. return from company only salary column
2. return * where salary > 40000
3. return * where salary > avg
4. return * of record where salary is maximum (hint: max)
5. create new table Company2 (for exmaple)
	INSERT all records from company1 to company2
		where salary < 80000
*/

-- 1
select SALARY from COMPANY;
-- 2
select * from COMPANY where SALARY > 40000;
-- 3
select * from COMPANY
where SALARY >  (SELECT avg(SALARY) from COMPANY);
-- 4
select * from COMPANY
where SALARY = (SELECT max(SALARY) from COMPANY);

-- 5
create table COMPANY2
(
    ID      INTEGER not null
        primary key autoincrement,
    NAME    TEXT    not null,
    AGE     INT     not null,
    ADDRESS CHAR(50),
    SALARY  REAL
);
INSERT INTO COMPANY2 (NAME,AGE,ADDRESS,SALARY)
SELECT NAME,AGE,ADDRESS,SALARY FROM COMPANY
WHERE COMPANY.SALARY < 80000;

-- proof that 5 works!
select count(*) bigger_eq_80k from COMPANY2
where SALARY >= 80000